package co.edu.udea.fsi.demofsi.component.employee.io.repository;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
