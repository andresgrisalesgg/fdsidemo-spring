package co.edu.udea.fsi.demofsi.component.employee.io.web.v1;

import co.edu.udea.fsi.demofsi.component.employee.io.web.v1.model.EmployeeListResponse;
import co.edu.udea.fsi.demofsi.component.employee.io.web.v1.model.EmployeeSaveRequest;
import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import co.edu.udea.fsi.demofsi.component.employee.service.EmployeeService;
import co.edu.udea.fsi.demofsi.component.employee.service.model.EmployeeSaveCmd;
import co.edu.udea.fsi.demofsi.component.shared.model.ResponsePagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/v1/employees")
public class EmployeeController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@Valid @NotNull @RequestBody EmployeeSaveRequest employeeToCreate){

        logger.debug("Begin create: employee to create = {}", employeeToCreate);

        final EmployeeSaveCmd employeeSaveCmd = EmployeeSaveRequest.toModel(employeeToCreate);

        final Employee employeeCreated = employeeService.create(employeeSaveCmd);

        URI location = UriComponentsBuilder.fromUriString("/api/v1/employees").path("/{id}")
                .buildAndExpand(employeeCreated.getId())
                .toUri();

        logger.debug("End create: employeeCreated = {}", employeeCreated);

        return ResponseEntity.created(location).build();
    }

    @GetMapping
    public ResponsePagination<EmployeeListResponse> findAll(@PageableDefault(size = 12,
            direction = Sort.Direction.DESC, sort = "id") Pageable pageable){

        logger.debug("Begin: findAll with pageable = {}", pageable);

        Page<Employee> employeesFound = employeeService.findAll(pageable);

        List<EmployeeListResponse> employeesFoundList = employeesFound.stream()
                .map(EmployeeListResponse::fromModel)
                .collect(Collectors.toList());

        logger.debug("End: findAll with page found = {}", employeesFound.getNumber());

        return ResponsePagination.fromObject(employeesFoundList, employeesFound.getTotalElements(),
                employeesFound.getNumber(), employeesFoundList.size());
    }
}
