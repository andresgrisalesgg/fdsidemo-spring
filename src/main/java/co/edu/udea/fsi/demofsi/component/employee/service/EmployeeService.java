package co.edu.udea.fsi.demofsi.component.employee.service;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import co.edu.udea.fsi.demofsi.component.employee.service.model.EmployeeSaveCmd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;

public interface EmployeeService {

    Employee create(@NotNull EmployeeSaveCmd employeeToCreateCmd);

    Page<Employee> findAll(@NotNull Pageable pageable);
}
