package co.edu.udea.fsi.demofsi.component.shared.web.exception;

public class BadRequestException extends RuntimeException {

    private static final String DESCRIPTION  = "Bad Request Exception code: (400)";

    public BadRequestException(String detail){
        super(DESCRIPTION + ". " + detail);
    }

}
