package co.edu.udea.fsi.demofsi.component.employee.io.web.v1.model;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeListResponse {

    private Long id;

    private String firstName;

    private String lastName;

    private String departmentName;

    private String position;

    private String telephone;

    private Boolean active;

    private LocalDateTime updateDate;

    public static EmployeeListResponse fromModel(Employee employee){
        return EmployeeListResponse.builder()
                .id(employee.getId())
                .firstName(employee.getFirstName())
                .lastName(employee.getLastName())
                .departmentName(employee.getDepartmentName())
                .active(employee.getActive())
                .position(employee.getPosition())
                .telephone(employee.getTelephone())
                .updateDate(employee.getUpdateDate())
                .build();
    }

}
