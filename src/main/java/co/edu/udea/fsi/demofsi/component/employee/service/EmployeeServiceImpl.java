package co.edu.udea.fsi.demofsi.component.employee.service;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import co.edu.udea.fsi.demofsi.component.employee.service.model.EmployeeSaveCmd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EmployeeGateway employeeGateway;

    public EmployeeServiceImpl(EmployeeGateway employeeGateway){
        this.employeeGateway = employeeGateway;
    }

    @Override
    public Employee create(@NotNull EmployeeSaveCmd employeeToCreateCmd) {

        logger.debug("Begin create employeeToCreateCmd = {}", employeeToCreateCmd);

        final Employee employeeToCreate = EmployeeSaveCmd.toModel(employeeToCreateCmd);

        final Employee employeeCreated = employeeGateway.save(employeeToCreate);

        logger.debug("End create employeeCreated = {}", employeeCreated);

        return employeeCreated;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Employee> findAll(@NotNull Pageable pageable) {

        logger.debug("Begin findAll with pageable = {}", pageable);

        Page<Employee> employeesFound = employeeGateway.findAll(pageable);

        logger.debug("End findAll with page found = {}", employeesFound.getNumber());

        return employeesFound;
    }
}
