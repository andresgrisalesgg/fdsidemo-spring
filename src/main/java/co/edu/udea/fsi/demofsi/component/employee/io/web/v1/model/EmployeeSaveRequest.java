package co.edu.udea.fsi.demofsi.component.employee.io.web.v1.model;

import co.edu.udea.fsi.demofsi.component.employee.service.model.EmployeeSaveCmd;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeSaveRequest {

    @NotEmpty
    @Size(min = 3)
    private String firstName;

    @NotEmpty
    @Size(min = 3)
    private String lastName;

    @NotEmpty
    private String departmentName;

    @NotEmpty
    private String position;

    @NotEmpty
    private String telephone;

    public static EmployeeSaveCmd toModel(EmployeeSaveRequest employeeSaveRequest){
        return EmployeeSaveCmd.builder()
                .firstName(employeeSaveRequest.getFirstName())
                .lastName(employeeSaveRequest.getLastName())
                .departmentName(employeeSaveRequest.getDepartmentName())
                .position(employeeSaveRequest.getPosition())
                .telephone(employeeSaveRequest.getTelephone())
                .build();
    }
}
