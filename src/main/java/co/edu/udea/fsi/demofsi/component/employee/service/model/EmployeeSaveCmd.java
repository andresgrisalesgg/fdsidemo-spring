package co.edu.udea.fsi.demofsi.component.employee.service.model;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Generated
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmployeeSaveCmd {

    @NotEmpty
    @Size(min = 3)
    private String firstName;

    @NotEmpty
    @Size(min = 3)
    private String lastName;

    @NotEmpty
    private String departmentName;

    @NotEmpty
    private String position;

    @NotEmpty
    private String telephone;

    public static Employee toModel(EmployeeSaveCmd employeeSaveCmd){
        return Employee.builder()
                .firstName(employeeSaveCmd.getFirstName())
                .lastName(employeeSaveCmd.getLastName())
                .departmentName(employeeSaveCmd.getDepartmentName())
                .position(employeeSaveCmd.getPosition())
                .telephone(employeeSaveCmd.getTelephone())
                .build();
    }
}
