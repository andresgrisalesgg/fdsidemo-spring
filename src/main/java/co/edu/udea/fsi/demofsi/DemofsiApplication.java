package co.edu.udea.fsi.demofsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemofsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemofsiApplication.class, args);
	}

}
