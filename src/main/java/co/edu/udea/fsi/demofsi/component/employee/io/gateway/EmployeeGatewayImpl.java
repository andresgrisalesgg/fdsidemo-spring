package co.edu.udea.fsi.demofsi.component.employee.io.gateway;

import co.edu.udea.fsi.demofsi.component.employee.io.repository.EmployeeRepository;
import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import co.edu.udea.fsi.demofsi.component.employee.service.EmployeeGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Repository
public class EmployeeGatewayImpl implements EmployeeGateway {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EmployeeRepository employeeRepository;

    public EmployeeGatewayImpl(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee save(@NotNull Employee employeeToCreate) {

        logger.debug("Begin save employeeToCreate = {}", employeeToCreate);

        final Employee employeeToBeCreated = employeeToCreate.toBuilder()
                .active(true)
                .createDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .build();

        final Employee employeeCreated = employeeRepository.save(employeeToBeCreated);

        logger.debug("End save employeeCreated = {}", employeeCreated);

        return employeeCreated;
    }

    @Override
    public Page<Employee> findAll(@NotNull Pageable pageable) {

        logger.debug("Begin findAll with pageable = {}", pageable);

        Page<Employee> employeesFound = employeeRepository.findAll(pageable);

        logger.debug("End findAll with page found = {}", employeesFound.getNumber());

        return employeesFound;
    }
}
