package co.edu.udea.fsi.demofsi.component.employee.service;

import co.edu.udea.fsi.demofsi.component.employee.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;


public interface EmployeeGateway {

    Employee save(@NotNull Employee employeeToCreate);

    Page<Employee> findAll(@NotNull Pageable pageable);
}
