package co.edu.udea.fsi.demofsi.component.shared.model;


import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Generated
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResponsePagination<T> implements Serializable {

    private List<T> result;

    private Long total;

    private int page;

    private int returnedRecords;

    public static <E> ResponsePagination<E> fromObject(List<E> result, Long total, Integer page,
                                                       Integer returnedRecords){
        return new ResponsePagination<>(result, total, page, returnedRecords);
    }

}
